# python import
import os

# django import
from django.core.exceptions import ImproperlyConfigured

# third party imports
from dotenv import load_dotenv

# loading environment variables from .env file using python-dotenv
load_dotenv()

# by default use development
ENV = os.getenv('ENV_ROLE', 'development')

try:
    # import local settings if present
    from .local import *  # noqa
except ImportError:
    pass
