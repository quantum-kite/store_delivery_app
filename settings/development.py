import os, dj_database_url
from .base import *

DEBUG = True

ALLOWED_HOSTS = []

DEVELOPMENT_APPS = [
    'django_extensions',
]

INSTALLED_APPS += DEVELOPMENT_APPS


DATABASES = {'default': dj_database_url.parse(os.getenv('DATABASE_URL'), 'django.contrib.gis.db.backends.postgis')}
