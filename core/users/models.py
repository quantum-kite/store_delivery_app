# python imports

# django imports
from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, PermissionsMixin
)
from django.contrib.gis.db import models as geo_models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# third party imports
from core.models import (TimestampedModel, UUIDModel)
from core.users.managers import UserManager


class GeoLocation(geo_models.Model):
    """
    We will store the latitude and longitude of the user. By using the lat and long
    will store the location of the user.
    """
    name = models.CharField(max_length=100)
    location = geo_models.PointField(srid=4326, null=True, blank=True)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=50)

    class Meta:
        abstract = True


class UserVerification(models.Model):
    """
    Will have the email and mobile verification.
    """
    is_email_verified = models.BooleanField(
        _('Is Email Verified'), default=False,
        help_text=_('Designates whether this email should be treated as active.')
    )

    is_mobile_verified = models.BooleanField(
        _('Is Mobile Verified'), default=False,
        help_text=_('Designates whether this mobile should be treated as active.')
    )

    class Meta:
        abstract = True


class User(AbstractBaseUser, UUIDModel, GeoLocation, UserVerification,
           PermissionsMixin, TimestampedModel):
    """
    Email and password are required other fields are optional
    """
    # Each `User` needs a human-readable unique identifier that we can use to
    # represent the `User` in the UI. We want to index this column in the
    # database to improve lookup performance.
    username = models.CharField(db_index=True, max_length=255, unique=True)

    # We also need a way to contact the user and a way for the user to identify
    # themselves when logging in. Since we need an email address for contacting
    # the user anyways, we will also use the email for logging in because it is
    # the most common form of login credential at the time of writing.
    email = models.EmailField(_('email address'), db_index=True, unique=True)

    # first name of the user and is optional
    first_name = models.CharField(_('First Name'), max_length=120, blank=True, null=True)

    # last name of the user and is optional
    last_name = models.CharField(_('Last Name'), max_length=120, blank=True, null=True)

    # mobile number of the user
    mobile_number = models.CharField(
        _('mobile number'), max_length=12, unique=True, blank=True, null=True
    )

    # When a user no longer wishes to use our platform, they may try to delete
    # there account. That's a problem for us because the data we collect is
    # valuable to us and we don't want to delete it. To solve this problem, we
    # will simply offer users a way to deactivate their account instead of
    # letting them delete it. That way they won't show up on the site anymore,
    # but we can still analyze the data.
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active or not.'
            'Unselect this instead of deleting accounts.'
        ),
    )

    # The `is_staff` flag is expected by Django to determine who can and cannot
    # log into the Django admin site. For most users, this flag will always be
    # falsed.
    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin site.')
    )

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    # location updated field. Will add the Date time in the updated location.
    location_updated_at = models.DateTimeField(null=True, blank=True)

    # The `USERNAME_FIELD` property tells us which field we will use to log in.
    # In this case, we want that to be the email field.
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'password']

    # Tells Django that the UserManager class defined above should manage
    # objects of this type.
    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        ordering = ('-date_joined',)

    def __str__(self):
        """
        Returns a string representation of this `User`.
        This string is used when a `User` is printed in the console.
        """
        return self.email

    # @property
    # def token(self):
    #     """
    #     Allows us to get a user's token by calling `user.token` instead of
    #     `user.generate_jwt_token().
    #     The `@property` decorator above makes this possible. `token` is called
    #     a "dynamic property".
    #     """
    #     return self._generate_jwt_token()

    def get_full_name(self):
        """
        This method is required by Django for things like handling emails.
        Typically, this would be the user's first and last name. Since we do
        not store the user's real name, we return their username instead.
        """
        return f'{self.first_name} {self.last_name}' \
            if self.first_name and self.last_name else ''

    def get_short_name(self):
        """
        This method is required by Django for things like handling emails.
        Typically, this would be the user's first name. Since we do not store
        the user's real name, we return their username instead.
        """
        return self.username

    # def _generate_jwt_token(self):
    #     """
    #     Generates a JSON Web Token that stores this user's ID and has an expiry
    #     date set to 60 days into the future.
    #     """
    #     dt = datetime.now() + timedelta(days=60)
    #
    #     token = jwt.encode({
    #         'id': self.pk,
    #         'exp': int(dt.strftime('%s'))
    #     }, settings.SECRET_KEY, algorithm='HS256')
    #
    #     return token.decode('utf-8')
