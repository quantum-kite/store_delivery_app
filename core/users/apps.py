from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UsersConfig(AppConfig):
    name = 'core.users'
    verbose_name = _('users')

    def ready(self):
        # don't remove this below import statement.
        # This tells django that there are signals to be triggered.
        import core.users.signals # noqa