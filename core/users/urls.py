# django imports
from django.urls import include, path

# third party imports
from rest_framework.authtoken import views

urlpatterns = [
    path('api-token-auth/', views.obtain_auth_token)
]
