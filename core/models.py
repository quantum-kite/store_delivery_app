# python imports
import uuid

# django imports
from django.db import models
from django.utils.translation import ugettext_lazy as _


class UUIDModel(models.Model):
    """
    An abstract base class model that makes primary key `id` as UUID
    instead of default auto incremented number.
    """
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)

    class Meta:
        abstract = True


class TimestampedModel(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields.
    """

    # A timestamp representing when this object was created.
    created_at = models.DateTimeField(_('created'), auto_now_add=True)

    # A timestamp representing when this object was last updated.
    updated_at = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        abstract = True

        # By default, any model that inherits from `TimestampedModel` should
        # be ordered in reverse-chronological order. We can override this on a
        # per-model basis as needed, but reverse-chronological is a good
        # default ordering for most models.
        ordering = ['-created_at', '-updated_at']
