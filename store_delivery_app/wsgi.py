"""
WSGI config for store_delivery_app project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

project_folder = os.path.expanduser('~/store_delivery_app')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'store_delivery_app.settings')

application = get_wsgi_application()
